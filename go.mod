module gitlab.com/fospathi/mechlaunch

go 1.20

require (
	gitlab.com/fospathi/mechane v0.0.0-20230417003624-92fede15df0c
	gitlab.com/fospathi/universal v0.0.0-20230417003025-64a421a94730
)

require (
	github.com/bcmills/unsafeslice v0.2.0 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	gitlab.com/fospathi/dryad v0.0.0-20230416143847-ed420eedd34d // indirect
	gitlab.com/fospathi/maf v0.0.0-20230417003027-12724b51e9e7 // indirect
	gitlab.com/fospathi/mico v0.0.0-20230417003029-09a46bbcfa58 // indirect
	gitlab.com/fospathi/phyz v0.0.0-20230417003036-46d2e35d15e1 // indirect
	gitlab.com/fospathi/wire v0.0.0-20230417003033-d87f22b3963f // indirect
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29 // indirect
	golang.org/x/mod v0.10.0 // indirect
)

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/mico => ../mico

replace gitlab.com/fospathi/mechane => ../mechane

replace gitlab.com/fospathi/phyz => ../phyz

replace gitlab.com/fospathi/wire => ../wire

replace gitlab.com/fospathi/universal => ../universal
