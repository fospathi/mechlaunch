package main

import (
	_ "embed"
	"log"
	"net/http"

	"gitlab.com/fospathi/mechane"
	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechrun"
	"gitlab.com/fospathi/mechane/mechui"
	"gitlab.com/fospathi/mechane/motif"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechlaunch/cmd"
	"gitlab.com/fospathi/mechlaunch/worlds"
)

func main() {
	var (
		abtMech  about.Mechane
		abtWorld about.World
		args     cmd.Args
		err      error
		lr       world.LaunchResources
		uiWeb    http.FileSystem
	)

	if args, err = cmd.Parse(cmd.ParseSpec{
		AllowWorld: true,
		Defaults:   cmd.Defaults,
	}); err != nil {
		log.Fatal(err)
	}

	if uiWeb, err = mechui.Root(); err != nil {
		log.Fatal(err)
	}

	if abtMech, err = about.NewMechane(mechane.GoMod); err != nil {
		log.Fatal(err)
	}

	if lr, err = worlds.Worlds[args.World](); err != nil {
		log.Fatal(err)
	}

	if abtWorld, err = lr.AboutWorld(); err != nil {
		log.Fatal(err)
	}

	mechrun.Run(mechrun.RunSpec{
		Address:    args.Addr(),
		Rasteriser: args.Rasteriser,
		UIWeb:      uiWeb,
		WorldWeb:   lr.WorldWeb,

		About: about.Software{
			Mechane: abtMech,
			World:   abtWorld,
		},
		Motif: motif.Default,
	}, args.World, lr.Run)
}
