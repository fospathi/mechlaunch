/*
Package mechsolo runs a standalone Mechane world.

A full Mechane world will also tend to make use of the various Go packages
designed for use in Mechane worlds.
*/
package mechsolo

import (
	_ "embed"
	"log"
	"net/http"

	"gitlab.com/fospathi/mechane"
	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechrun"
	"gitlab.com/fospathi/mechane/mechui"
	"gitlab.com/fospathi/mechane/motif"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechlaunch/cmd"
)

// Run the given Mechane world with the given world name from your app's main()
// function/thread.
//
// Run shall only be called from the main thread, that is, not from a goroutine!
func Run(name string, lr world.LaunchResources) {
	var (
		abtMech  about.Mechane
		abtWorld about.World
		args     cmd.Args
		err      error
		uiWeb    http.FileSystem
	)

	if args, err = cmd.Parse(cmd.ParseSpec{
		AllowWorld: false,
		Defaults:   cmd.Defaults,
	}); err != nil {
		log.Fatal(err)
	}

	if uiWeb, err = mechui.Root(); err != nil {
		log.Fatal(err)
	}

	if abtMech, err = about.NewMechane(mechane.GoMod); err != nil {
		log.Fatal(err)
	}

	if abtWorld, err = lr.AboutWorld(); err != nil {
		log.Fatal(err)
	}

	mechrun.Run(mechrun.RunSpec{
		Address:    args.Addr(),
		Rasteriser: args.Rasteriser,
		UIWeb:      uiWeb,
		WorldWeb:   lr.WorldWeb,

		About: about.Software{
			Mechane: abtMech,
			World:   abtWorld,
		},
		Motif: motif.Default,
	}, name, lr.Run)
}
