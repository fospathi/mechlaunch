/*
Package cmd parses the command-line flags of the Mechane app.
*/
package cmd

import (
	"errors"
	"flag"
	"fmt"
	"net/url"
	"strconv"

	"gitlab.com/fospathi/mechlaunch/worlds"
	"gitlab.com/fospathi/universal/mech"
)

// Args contains either default values or, when given, values passed to the app
// as command-line flags.
type Args struct {
	Hostname   string
	Port       int
	Rasteriser string
	World      string
}

// Addr is the web address of the receiver app and consists of its hostname and
// port.
func (a Args) Addr() string {
	return a.Hostname + ":" + strconv.Itoa(a.Port)
}

var Defaults = Args{
	Hostname:   mech.ServerHostname,
	Port:       int(mech.ServerPort),
	Rasteriser: mech.EmbeddedFlag,
	World:      worlds.DefaultWorld,
}

// ParseSpec customises the parsing of Mechane command-line flags.
type ParseSpec struct {
	AllowWorld bool // AllowWorld decides whether the 'world' command-line flag is allowed.
	Defaults   Args
}

// Parse the command-line flags and use the given default flag values.
func Parse(spec ParseSpec) (Args, error) {

	defaults := spec.Defaults

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of mechane:\n")
		flag.PrintDefaults()
	}

	hostname := flag.String(
		"hostname",
		defaults.Hostname,
		"The hostname part of the Mechane server's host",
	)

	msg := "The port part of the Mechane server's host.\n" +
		"Specify 0 to use an ephemeral port (default %v)"
	port := flag.Int(
		"port",
		defaults.Port,
		fmt.Sprintf(msg, mech.ServerPort),
	)

	msg = "Rasterise scenes using this rasteriser. Can be either:\n" +
		"* the URL of an existing wireserver or\n" +
		"* \"%v\" to create a private rasteriser"
	rasteriser := flag.String(
		"rasteriser",
		defaults.Rasteriser,
		fmt.Sprintf(msg, mech.EmbeddedFlag),
	)

	var world *string
	if spec.AllowWorld {
		world = flag.String(
			"world",
			defaults.World,
			"Run the Mechane world with this name",
		)
	}

	flag.Parse()

	if spec.AllowWorld {
		if _, ok := worlds.Worlds[*world]; !ok {
			return Args{}, errors.New("no such world: " + *world)
		}
	} else {
		var empty string
		world = &empty
	}

	if *rasteriser != mech.EmbeddedFlag {
		if _, err := url.ParseRequestURI(*rasteriser); err != nil {
			return Args{}, errors.New("invalid rasteriser: " + *rasteriser)
		}
	}

	return Args{
		Hostname:   *hostname,
		Port:       *port,
		Rasteriser: *rasteriser,
		World:      *world,
	}, nil
}
