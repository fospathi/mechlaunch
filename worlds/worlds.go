/*
Package worlds contains Mechane's inbuilt selection of worlds available by
default.

A world listed here can be run by providing the world name as a command-line
flag to Mechane.
*/
package worlds

import (
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d"
	"gitlab.com/fospathi/mechane/worlds/noodlewood"
	"gitlab.com/fospathi/mechane/worlds/solarsystem"
)

// DefaultWorld to run in the absence of a specified world.
const DefaultWorld = test3d.WorldName

// Worlds that are available to be run by Mechane by default.
//
// A map key is a world name that can be provided as a command-line flag to
// Mechane to run that world.
var Worlds = map[string]func() (world.LaunchResources, error){
	test3d.WorldName: test3d.LaunchResources,

	noodlewood.WorldName:  noodlewood.LaunchResources,
	solarsystem.WorldName: solarsystem.LaunchResources,
}
